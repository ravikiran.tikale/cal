var num1, num2;
function add()
{
	num1 = document.getElementById("firstNumber").value;
    num2 = document.getElementById("secondNumber").value;
    document.getElementById("result").innerHTML = +num1 + +num2;;
}

function sub()
{
    num1 = document.getElementById("firstNumber").value;
    num2 = document.getElementById("secondNumber").value;
    document.getElementById("result").innerHTML = num1 - num2;
}

function mul()
{
    num1 = document.getElementById("firstNumber").value;
    num2 = document.getElementById("secondNumber").value;
    document.getElementById("result").innerHTML = num1 * num2;
}

function div() 
{ 
    num1 = document.getElementById("firstNumber").value;
    num2 = document.getElementById("secondNumber").value;
	document.getElementById("result").innerHTML = num1 / num2;
}

function mod() 
{ 
    num1 = document.getElementById("firstNumber").value;
    num2 = document.getElementById("secondNumber").value;
	document.getElementById("result").innerHTML = num1 % num2;
}

function oe() {
	var input = document.getElementById("firstNumber").value;
	var firstNumber = parseInt(input);
	if (firstNumber % 2 == 0 ) {
		document.getElementById("result").innerHTML = firstNumber + " is even number.";
	}
	else {
		document.getElementById("result").innerHTML = firstNumber + " is odd number.";
	}
}

function prime(){
                
    var num = document.getElementById('firstNumber').value;
    var c = 0;
    
    for (i = 1; i <= num; i++) {
                
        if (num % i == 0) {
            c = c + 1;
        }
    }

    if (c == 2) {
        document.getElementById('result').innerHTML = num + ' is a prime number';
    }else{
        document.getElementById('result').innerHTML = num + ' is not a prime number';
    }
}

function fact(){

	var i, fact;
	fact = 1;
	num1 = Number(document.getElementById("firstNumber").value);
	for(i = 1; i <= num1; i++)  
	{
		fact = fact*i;
	}  
	document.getElementById("result").innerHTML = fact;
}
